#include <iostream>
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

typedef struct _Nodo {
	int dato;
	struct _Nodo *izq;
	struct _Nodo *der;
	struct _Nodo *padre;
} Nodo;


class Arbol {
	public:
		/* Constructor */
		Arbol();

		/* Metodos */
		Nodo *crearNodo (int, Nodo *);
		void insertarNodo (Nodo *&, int, Nodo *); //pasar el arbol por ref
		void mostrarArbol (Nodo *, int);
		bool busqueda (Nodo *, int);
		void eliminar (Nodo *, int); //busca elemeto a eliminar
		//eliminarNodo: elimina elemento, se le pasa el elem encontrado
		void eliminarNodo (Nodo *);
		Nodo *minimo (Nodo *);
		void reemplazar (Nodo *, Nodo *); //para eliminar nodo con 1 hijo
		//destruirNodo: destruye un nodo; olvida hijos y se elimina
		void destruirNodo (Nodo *);
		void graficar (Nodo *); //crea archivo del grafo

	//private:
		void preOrden (Nodo *);
		void inOrden (Nodo *);
		void postOrden (Nodo *);

};
#endif