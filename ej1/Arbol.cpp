#include <iostream>
#include <fstream>
#include "Arbol.h"
#include "Grafo.h"

Arbol::Arbol(){}

/* fx para crear un nuevo nodo */
Nodo* Arbol::crearNodo(int n, Nodo *padre) {
	Nodo *nuevo_nodo; //nuevo_nodo es un nuevo nodo

	nuevo_nodo = new Nodo(); //Reserva memoria
	nuevo_nodo->dato = n;
	nuevo_nodo->padre = padre;
	nuevo_nodo->izq = NULL;
	nuevo_nodo->der = NULL;

	return nuevo_nodo;
}

/* fx para insertar elem en el arbol */
void Arbol::insertarNodo(Nodo *&arbol, int n, Nodo *padre){
	/* Si el arbol esta vacio */
	if (arbol == NULL) {
		/* CREA UN NUEVO NODO, se le pasa el elemento e indica su padre */
		Nodo *nuevo_nodo = crearNodo(n, padre);
		arbol = nuevo_nodo; //Almacena dato en la raiz
		cout << " Elemento insertado con exito"<<endl;

	} else { //Arbol tiene un nodo o raiz
		/* Obtener el valor de la raiz*/
		int valorRaiz = arbol->dato;

		/* Si el elemento es menor a la raiz, insertar en izq
		 * e indicar su padre (referenciado por arbol) */
		if (n < valorRaiz) {
			insertarNodo(arbol->izq, n, arbol);

		}
		/* Si el elemento es mayor a la raiz, insertar en der
		 * e indicar su padre (referenciado por arbol) */
		else if (n > valorRaiz) { 
			insertarNodo(arbol->der, n, arbol);

		} 
		/* Elemento repetido */
		else {
			cout << "El valor " << n << " ya existe en el árbol" << endl;
		}
	}
}

/* fx para mostrar arbol completo de forma horizontal */
void Arbol::mostrarArbol(Nodo *arbol, int cont){ 
	/* Es de tipo puntero porq no voy a cambiar ningun valor */
	
	/* Contador no cuenta la cant de nodos, es solo para separar (ordenar) la
	 * impresion entre nodos */

	if (arbol == NULL) {
		/* Arbol vacio */
		return;

	} else {
		/* Empieza por el lado derecho */
		mostrarArbol (arbol->der, cont+1 );
		for (int i = 0; i < cont; ++i) {
			cout << "   "; //3 espacios pensando en nodos de 3 digitos
		}

		cout << arbol->dato << endl;
		/* Continuar con el lado izquierdo */
		mostrarArbol(arbol->izq,cont+1 );
	}
}

/* fx para buscar un elemento en el arbol */
bool Arbol::busqueda(Nodo *arbol, int n){
	if (arbol == NULL) {
		/* Arbol vacio */
		return false;
	} else if (arbol->dato == n) {
		/* Si el nodo actual es igual al elemento */
		return true;
	} else if (n < arbol->dato) {
		/* Si la busqueda es menor al nodo, analiza rama izq */
		return busqueda(arbol->izq, n);
	} else {
		/* Si la busqueda es mayor al nodo, analiza rama der */
		return busqueda(arbol->der, n);
	}
}

/* fx para recorrido preOrden */
void Arbol::preOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero la raiz, luego el subarbol izq,
		 * y despues el der */
		cout << arbol->dato << " - ";
		preOrden(arbol->izq);
		preOrden(arbol->der);
	}
}


/* fx para recorrido inOrden */
void Arbol::inOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero el subarbol izq, luego la raiz
		 * y despues el subarbol der */
		inOrden(arbol->izq);
		cout << arbol->dato << " - ";
		inOrden(arbol->der);
	}
}

/* fx para recorrido postOrden */
void Arbol::postOrden(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		/* Primero el subarbol izq, luego el der
		 * y despues la raiz */
		postOrden(arbol->izq);
		postOrden(arbol->der);
		cout << arbol->dato << " - ";
	}
}

/* fx para eliminar un nodo */
void Arbol::eliminar(Nodo *arbol, int n){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} 
	/* Recorrer para ver si existe el elemento */

	/* 1. Si existe y es menor al nodo actual,
	 * significa q esta por la izq del arbol
	 * 2. Si existe y es mayor al nodo actual.
	 * significa q esta por la der del arbol
	 * 3. Encontró el valor, eliminamos*/
	else if (n < arbol->dato) { //Si es menor
		eliminar(arbol->izq, n); //Busca por la izq
	}
	else if (n > arbol->dato) { //Si es mayor
		eliminar(arbol->der, n); //Busca por la der
	} else { //Encontro el valor
		eliminarNodo(arbol);
	}
}

/* fx para determinar el nodo mas a la izq posible */
Nodo* Arbol::minimo(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return NULL;
	}

	if (arbol->izq) { 
		/* Si tiene hijo izq */
		return minimo(arbol->izq); //Buscar la parte mas izq posible
	} else {
		/* Si no tiene hijo izq */
		return arbol; //Retorna el mismo nodo
	}
}

/* fx para reemplazar 2 nodos (uno por el otro) */
void Arbol::reemplazar(Nodo *arbol, Nodo *nuevoNodo){
	/* Primero necesitamos el padre de el nodo q vamos a eliminar */
	
	/* Si el arbol tiene padre, entonces asignarle su nuevo hijo */
	if (arbol->padre) {

		/* Verificar si el nodo a eliminar es izq o der */
		if (arbol->dato == arbol->padre->izq->dato) {
			/* Si el nodo a eliminar sea un hijo izq o tenga un hijo izq,
			 * se reemplaza */
			arbol->padre->izq = nuevoNodo;
		} else if (arbol->dato == arbol->padre->der->dato) {
			/* Si el nodo a eliminar sea un hijo der o tenga un hijo der,
			 * se reemplaza */
			arbol->padre->der = nuevoNodo;
		}
	}
	/* Si el nuevo nodo existe, se le asigna su nuevo padre */
	if (nuevoNodo) {
		nuevoNodo->padre = arbol->padre;
	}
}

/* fx para destruir un nodo */ 
void Arbol::destruirNodo(Nodo *nodo) { //Se le pasa el nodo a destruir
	/* Primero q se olvide de sus hijos */
	nodo->izq = NULL; //Si tenia hijo izq, ya no lo tiene
	nodo->der = NULL; //Si tenia hijo der, ya no lo tiene

	/* Eliminar nodo */
	delete nodo;
}

/* fx para eliminar el nodo encontrado */
void Arbol::eliminarNodo(Nodo *nodoEliminar){
	/* Verificar si el nodo es hoja o tiene hijos (1 o 2) */

	/* Borrar nodo con 2 subarboles hijos */
	if (nodoEliminar->izq && nodoEliminar->der) {
		/* Ir a hijo derecho y luego a la parte mas izq posible */
		
		/* Necesita el valor minimo de estos (fx minimo)*/
		Nodo *menor = minimo(nodoEliminar->der);
		/* Ahora reemplazar (igualar) nodo anterior */
		nodoEliminar->dato = menor->dato;
		eliminarNodo(menor); //Eliminar el nodo en sí
	}

	/* Borrar nodo con 1 hijo (o izq o der) */
	else if (nodoEliminar->izq) { //Si tiene hijo izq
		/* Nodo hijo toma el lugar del padre (reemplazar) y eliminamos el
		 * nodo solicitado. Llamar a la funcion reemplazar */
		reemplazar(nodoEliminar, nodoEliminar->izq);
		/* Eliminar (fx destruir) el nodo a eliminar, que ya no tiene padre
		 * pero es padre */
		destruirNodo(nodoEliminar);
	} 
	else if (nodoEliminar->der) { //Si tiene hijo der
		reemplazar(nodoEliminar, nodoEliminar->der);
		destruirNodo(nodoEliminar);
	}

	/* Borrar un nodo hoja (no tiene hijos) */
	else {
		/* Utilizar la fx reemplazar. Reemplazando el nodo a eliminar por
		 * NULL, de forma q ya no tenga padre y se destruya */
		reemplazar(nodoEliminar, NULL);
		destruirNodo(nodoEliminar);
	}
}

/* fx para generar imagen del arbol */
void Arbol::graficar(Nodo *arbol){
	if (arbol == NULL) {
		/* Arbol vacio */
		return;
	} else {
		cout << " Iniciando creacion del grafico" << endl;
		Grafo *grap = new Grafo(arbol);
	}
}