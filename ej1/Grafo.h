#include <iostream>
#include <fstream>
#include "Arbol.h"

using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

/* Archivo .h con todas las cabeceras
typedef struct _Nodo {
	int info;
	struct _Nodo *izq;
	struct _Nodo *der;
} Nodo; */

class Grafo {

	public:
		/* constructor*/
		Grafo(Nodo *nodo);

		/* Métodos de la clase Grafo */

		//recorrerArbol: recorre el archivo grafo y escribe en él
		void recorrerArbol (Nodo *, ofstream &);

};
#endif