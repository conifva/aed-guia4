# AED-Guia4

Actividades cuarta guía de AED 2019
(Primera guía U2: AAB)


# Guia4-UII

Tema: Árboles Binarios de Búsqueda

> Programa que cree un árbol binario de búsqueda (número enteros). Éste tipo de árbol tiene la caracterı́stica de mantener sus elementos ordenados y sin repetirse.
> 
> El programa permite las siguientes operaciones básicas sobre la estructura:
>	- Insertar número
>	- Eliminar número buscado
>	- Modificar un elemento buscado (eliminar valor viejo e insertar el nuevo valor)
>	- Mostrar el contenido del árbol en Preorden, Inorden y Posorden
>	- Generar el grafo correspondiente de la estructura creada mediante la herramienta Graphiz (http://www.graphviz.org/). El paquete en Debian GNU/Linux es graphviz
>
> Para ejecutar los comandos que generan el grafo y lo visualiza, pueden utilizar la función system().
> 
> A continuación se presenta un ejemplo respecto a como se ordenan los ingresos de valores.
> 
>	Para el ingreso de los siguientes valores: 120, 87, 140, 43, 99, 130, 22, 65, 93, 135, 56. Los resultados de los diferentes recorridos serı́an:
>	```
>	— Preorden—
>	120 - 87 - 43 - 22 - 65 - 56 - 99 - 93 - 140 - 130 - 135
>
>	— Inorden—
>	22 - 43 - 56 - 65 - 87 - 93 - 99 - 120 - 130 - 135 - 140
>
>	— Posorden—
>	22 - 56 - 65 - 43 - 93 - 99 - 87 - 135 - 130 - 140 - 120
>	```
> 
> El código que genera el grafo es almacenado en un archiivo de txt (grafo.txt). 
>
> Para generar el grafo se utiliza el comando: `` `dot -Tpng -ografo.png grafo.txt` ``, y para visualizarlo se puede invoca el programa eog, como: `` `eog grafo.png` ``
> 


# Obtención del Programa
Clonar repositorio, ingresar a la carpeta y ejecutar por terminal:
```
g++ untitled.cpp Arbol.cpp -o ejecutable
make
./ejecutable
```


# Acerca de 
El programa presenta inicialmente un menú con 8 opciones. 

La primera consiste en agregar elementos (numeros enteros) a un árbol binario inicializado al momento de ejecutar el programa, este valor es evaluado para verificar que ninguno de los nodos del árbol se repita y almacenado como un nuevo nodo. La opción 2 imprime por terminal los nodos del arbol en posición horizontal. A continuación se muestra una breve ejemplo.

La imagen numero 1 muestra un árbol binario que servirá como templado para ver la funcionalidad de la opción numero 2 del programa.
![Imagen 1](arbolOriginal.png)

La imagen número 2 muestra como se ve el árbol binario de la figura anterior generado por el programa. 

![Imagen 2](arbolTerminal.png)

La imagen número 3 muestra el árbol generado con el software Graphviz a partir de los datos ingresados con un giro de 90° a la izquierda. Evidenciando la correcta impresión del árbol binario en posisción horizontal.
![Imagen 3](terminal+png.png)

La imagen número 4 muestra el árbol generado con el software Graphviz a partir de los datos ingresados. 
![Imagen 4](ejgrafo.png)

Por otra parte la tercera alternativa del programa permite determinar la existencia de un determinado valor, ingresado por el usuario, en el árbol. La cuarta imprime los tres tipos de recorridos en profundidad al árbol; Preorden, Postorden e Inorden. La opción cinco ofrece la eliminación de algún nodo ingresado, analizando si el nodo es de tipo padre u hoja (posee o no hijos). La sexta elección permite la modificación de un nodo, donde este es: en primer lugar eliminado y posteriormente se solicita el ingreso de un nuevo nodo. La penúltima acción que se puede realizar es generar una imagen formato "png" a partir de los datos ingresados a través de el software Graphviz, como se muestra en la figura 4.

Finalmente la octava opción permite al usuario cerrar el programa.


# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Colección de software Graphviz
- Visor de imágenes EOG (Eye of GNOME)
- Compilador GNU C++ (g++)


# Construccion
Construido y probado con:
- Ubuntu 18.04.03 LTS
- gcc y g++ version 7.4.0
- GNU Make 4.1
- graphviz version 2.38.0
- EOG 3.28.1
- Editor utilizado: Sublime Text


# Autor
Constanza Valenzuela